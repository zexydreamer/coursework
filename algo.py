def main():
    temp = read_input()
    n = temp[1]
    matrix = temp[0]
    g = make_matrix(n)
    fill_matrix(matrix, g)
    target = [x for x in g.keys() if x[0] == x[1]]
    for i in range(n):
        for el in target:
            path = list()
            if (i + 1, i + 1) != el and search(g, (i + 1, i + 1), el, path):
                if len(path) > 2:
                    return 'N'
    return 'Y'


def read_input():
    with open('in.txt', 'r') as source:
        i = int(source.readline())
        n = int(source.readline())
        matrix = list()
        while i > 0:
            s = list()
            for _ in range(n):
                s.append(source.readline())
            t = list()
            k = 0
            for row in s:
                t.append(list())
                for el in row.split(' '):
                    t[k].append(int(el))
                k += 1
            matrix.append(t)
            i -= 1
    return matrix, n


def fill_matrix(matrix: list, g: dict):
    for k in range(len(matrix)):
        for row in g:
            for column in g[row]:
                if matrix[k][row[0] - 1][row[1] - 1] == 1 and matrix[k][column[0] - 1][column[1] - 1] == 1:
                    g[row][column] = 1


def make_matrix(n: int):
    result = dict()
    for i in range(n):
        for j in range(n):
            result[(i + 1, j + 1)] = dict()
            for l in range(n):
                for m in range(n):
                    result[(i + 1, j + 1)][(l + 1, m + 1)] = 0
    return result


def search(graph: dict, start: tuple, stop: tuple, path: list):
    if start == stop:
        path.append(stop)
        return True
    path.append(start)
    if start < stop:
        for el in graph[start]:
            if graph[start][el] == 1 and el != start and el > start:
                if search(graph, el, stop, path):
                    return True
    return False


if __name__ == '__main__':
    with open('out.txt', 'w') as output:
        output.write(f'{main()}')
